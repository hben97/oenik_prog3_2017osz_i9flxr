﻿// <copyright file="JatekFrameWorkElement.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace I9Flxr_FelevesBeadandoProg3.View
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Threading;
    using I9Flxr_FelevesBeadandoProg3.BL;
    using I9Flxr_FelevesBeadandoProg3.Model;

    /// <summary>
    /// Egy megjeleníthető UI
    /// </summary>
    public class JatekFrameWorkElement : FrameworkElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="JatekFrameWorkElement"/> class.
        /// Létrehoz egy UI elemet
        /// </summary>
        public JatekFrameWorkElement()
        {
            this.Loaded += this.JatekFrameWorkElement_Loaded;
            this.KeyDown += this.JatekFrameWorkElement_KeyDown;
        }

        /// <summary>
        /// Gets or sets : megadja a Jatekot
        /// </summary>
        public Jatek Jatek { get; set; }

        /// <summary>
        /// Gets or sets : megadja az UI Element időzítőjét
        /// </summary>
        public DispatcherTimer Idozito { get; set; }

        /// <summary>
        /// Inicializálja az UI elemet
        /// </summary>
        public void Init()
        {
            if (this.Jatek == null)
            {
                return;
            }

            this.Jatek.Init();

            this.InvalidateVisual();
        }

        /// <summary>
        /// A kirajzolásért felelős
        /// </summary>
        /// <param name="drawingContext">Vizuális megjelenítőréteg</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            this.HatterKirajzol(drawingContext);

            this.JatekosKirajzolasa(drawingContext);

            this.AkadalyokKirajzolasa(drawingContext);
            this.KulonLegesMezokKirajzolasa(drawingContext);
            this.LovesekKirajzolasa(drawingContext);
            this.SzornyekKirajzolasa(drawingContext);

            if (this.Jatek.JatekVege)
            {
                SzovegKirajzol(drawingContext, "Vége a játéknak :(" + Environment.NewLine + this.Jatek.Jatekos.Pont + " pont összesen" + Environment.NewLine + Environment.NewLine + "ESC az új játékhoz", DinamikusAdatok.ViewModel.BelepesiPontSzelesseg / 2, 30, 30, Brushes.Red, TextAlignment.Center);
            }
            else
            {
                SzovegKirajzol(drawingContext, this.Jatek.Jatekos.Elet + " élet", DinamikusAdatok.ViewModel.BelepesiPontSzelesseg / 2, 30, 20, Brushes.Blue, TextAlignment.Center);
                SzovegKirajzol(drawingContext, this.Jatek.Jatekos.Pont + " pont", DinamikusAdatok.ViewModel.BelepesiPontSzelesseg - 66, 30, 20, Brushes.Blue, TextAlignment.Center);
                SzovegKirajzol(drawingContext, Environment.NewLine + this.Jatek.UtolsoSzoveg, this.Jatek.Jatekos.X + this.Jatek.Jatekos.Szelesseg, this.Jatek.Jatekos.Y - this.Jatek.Jatekos.Magassag, (int)this.Jatek.Jatekos.Szelesseg / 2, Brushes.Red, TextAlignment.Left);
            }

            if (this.Jatek.Menuben)
            {
                this.MenuKirajzolasa(drawingContext);
            }
        }

        private static void SzovegKirajzol(DrawingContext drawingContext, string szoveg, double x, double y, int betuMeret, Brush kinezet, TextAlignment textAlignment)
        {
            FormattedText multiplierText = new FormattedText(
                    szoveg,
                    CultureInfo.CurrentUICulture,
                    FlowDirection.LeftToRight,
                    new Typeface("Verdana"),
                    betuMeret,
                    kinezet);
            multiplierText.TextAlignment = textAlignment;

            drawingContext.DrawText(
                multiplierText,
                new Point(x, y));
        }

        private void JatekFrameWorkElement_KeyDown(object sender, KeyEventArgs e)
        {
          if (!this.Jatek.Beallitasokban)
            {
                if ((e.Key == Key.Enter || e.Key == Key.Up) && !this.Jatek.Menuben)
                {
                    JatekosInterakciokStatic.Ugrik(this.Jatek.Jatekos);
                }
                else if ((e.Key == Key.RightShift || e.Key == Key.Down) && !this.Jatek.Menuben)
                {
                    JatekosInterakciokStatic.LeaFoldre(this.Jatek.Jatekos);
                }
                else if (e.Key == Key.Space && !this.Jatek.Menuben)
                {
                    Loves loves = JatekosInterakciokStatic.Loves(this.Jatek.Jatekos);
                    this.Jatek.Lovesek.Add(loves);
                }
                else if (e.Key == Key.Escape)
                {
                    this.Felfuggeszt();
                }
            }

           if (this.Jatek.Beallitasokban)
            {
                if (e.Key == Key.Left)
                {
                    int sz = StatikusAdatok.JatekosKepFileLista.IndexOf(DinamikusAdatok.ViewModel.JatekosKepFile);
                    if (sz - 2 >= 0)
                    {
                        DinamikusAdatok.ViewModel.JatekosKepFile = StatikusAdatok.JatekosKepFileLista[sz - 2];
                        DinamikusAdatok.ViewModel.JatekosHalottKep = StatikusAdatok.JatekosKepFileLista[StatikusAdatok.JatekosKepFileLista.IndexOf(DinamikusAdatok.ViewModel.JatekosKepFile) + 1];
                    }
                }
                else if (e.Key == Key.Right)
                {
                    int sz = StatikusAdatok.JatekosKepFileLista.IndexOf(DinamikusAdatok.ViewModel.JatekosKepFile);
                    if (sz + 2 < StatikusAdatok.JatekosKepFileLista.Count)
                    {
                        DinamikusAdatok.ViewModel.JatekosKepFile = StatikusAdatok.JatekosKepFileLista[sz + 2];
                        DinamikusAdatok.ViewModel.JatekosHalottKep = StatikusAdatok.JatekosKepFileLista[StatikusAdatok.JatekosKepFileLista.IndexOf(DinamikusAdatok.ViewModel.JatekosKepFile) + 1];
                    }
                }
                else if (e.Key == Key.Enter)
                {
                    this.Jatek.Beallitasokban = false;
                }
            }
           else
            {
                if (this.Jatek.Menuben && this.Jatek.UJJatek)
                {
                    if (e.Key == Key.Down)
                    {
                        if (DinamikusAdatok.ViewModel.KijeloltMenuElemSzama + 1 < StatikusAdatok.JatekMenuElemekUJJatek.Count)
                        {
                            DinamikusAdatok.ViewModel.KijeloltMenuElemSzama++;
                        }
                    }
                    else if (e.Key == Key.Up)
                    {
                        if (DinamikusAdatok.ViewModel.KijeloltMenuElemSzama - 1 >= 0)
                        {
                            DinamikusAdatok.ViewModel.KijeloltMenuElemSzama--;
                        }
                    }
                    else if (e.Key == Key.Enter)
                    {
                        switch (DinamikusAdatok.ViewModel.KijeloltMenuElemSzama)
                        {
                            case 0: this.UjJatekInditasa(); break;
                            case 1: this.Jatek.Beallitasokban = true; break;
                            case 2: Environment.Exit(Environment.ExitCode); break;
                            default:
                                break;
                        }
                    }
                }
                else if (this.Jatek.Menuben && !this.Jatek.UJJatek)
                {
                    if (e.Key == Key.Down)
                    {
                        if (DinamikusAdatok.ViewModel.KijeloltMenuElemSzama + 1 < StatikusAdatok.JatekMenuElemekPause.Count)
                        {
                            DinamikusAdatok.ViewModel.KijeloltMenuElemSzama++;
                        }
                    }
                    else if (e.Key == Key.Up)
                    {
                        if (DinamikusAdatok.ViewModel.KijeloltMenuElemSzama - 1 >= 0)
                        {
                            DinamikusAdatok.ViewModel.KijeloltMenuElemSzama--;
                        }
                    }
                    else if (e.Key == Key.Enter)
                    {
                        switch (DinamikusAdatok.ViewModel.KijeloltMenuElemSzama)
                        {
                            case 0: this.Jatek.Menuben = false; this.Felfuggeszt(); break;
                            case 1: this.UjJatekInditasa(); break;
                            case 2: this.Jatek.Beallitasokban = true; break;
                            case 3: Environment.Exit(Environment.ExitCode); break;
                            default:
                                break;
                        }
                    }
                }
            }

            this.InvalidateVisual();
        }

        private void JatekFrameWorkElement_Loaded(object sender, RoutedEventArgs e)
        {
            this.Focusable = true;
            this.Focus();
            this.Idozito = new DispatcherTimer();
            this.Idozito.Interval = StatikusAdatok.JatekFrameWorkElementIdozitoSpan;
            this.Idozito.Tick += this.Idozito_Tick;
            this.Idozito.Start();
        }

        private void Idozito_Tick(object sender, EventArgs e)
        {
            if (this.Jatek != null)
            {
                this.Jatek.Halad();

                if (this.Jatek.JatekVege)
                {
                    this.Idozito.Stop();
                }

                this.InvalidateVisual();
            }
        }

        private void MenuKirajzolasa(DrawingContext drawingContext)
        {
            if (this.Jatek.UJJatek)
            {
                int szam = StatikusAdatok.JatekMenuElemekUJJatek.Count;
                for (int i = 0; i < szam; i++)
                {
                    Rect rect = new Rect(0, 5 + (i * 75), DinamikusAdatok.ViewModel.BelepesiPontSzelesseg, 70);
                    Brush brush = Brushes.White;
                    if (i == DinamikusAdatok.ViewModel.KijeloltMenuElemSzama)
                    {
                        brush = Brushes.Black;
                    }

                    drawingContext.DrawRectangle(brush, null, rect);
                    SzovegKirajzol(drawingContext, StatikusAdatok.JatekMenuElemekUJJatek[i], 0, 5 + (i * 75), 30, Brushes.Red, TextAlignment.Left);
                }
            }
            else
            {
                int szam = StatikusAdatok.JatekMenuElemekPause.Count;
                for (int i = 0; i < szam; i++)
                {
                    Rect rect = new Rect(0, 5 + (i * 75), DinamikusAdatok.ViewModel.BelepesiPontSzelesseg, 70);
                    Brush brush = Brushes.White;
                    if (i == DinamikusAdatok.ViewModel.KijeloltMenuElemSzama)
                    {
                        brush = Brushes.Black;
                    }

                    drawingContext.DrawRectangle(brush, null, rect);
                    SzovegKirajzol(drawingContext, StatikusAdatok.JatekMenuElemekPause[i], 0, 5 + (i * 75), 30, Brushes.Red, TextAlignment.Left);
                }
            }

            if (this.Jatek.Beallitasokban)
            {
                Rect rect;

                rect = new Rect((DinamikusAdatok.ViewModel.BelepesiPontSzelesseg / 2) - (DinamikusAdatok.ViewModel.JatekosSzelesseg * 2 / 2), 10, DinamikusAdatok.ViewModel.JatekosSzelesseg * 2, DinamikusAdatok.ViewModel.JatekosMagassag * 2);

                Uri kepUri = null;
                BitmapImage kep = null;
                if (Uri.TryCreate(DinamikusAdatok.ViewModel.JatekosKepFile, UriKind.RelativeOrAbsolute, out kepUri))
                {
                    kep = new BitmapImage(kepUri);
                }

                drawingContext.DrawImage(kep, rect);

                double j1 = (DinamikusAdatok.ViewModel.BelepesiPontSzelesseg / 2) - (DinamikusAdatok.ViewModel.JatekosSzelesseg * 2 / 2);
                double sz = DinamikusAdatok.ViewModel.BelepesiPontSzelesseg / 10;
                double ma = DinamikusAdatok.ViewModel.BelepesiPontMagassag / 10;
                rect = new Rect((j1 / 2) - (sz / 2), 10 + (DinamikusAdatok.ViewModel.JatekosMagassag - (ma / 2)), sz, ma);
                drawingContext.DrawRoundedRectangle(Brushes.Red, null, rect, 15, 15);
                SzovegKirajzol(drawingContext, "<<<", rect.X, rect.Y, (int)sz / 3, Brushes.Black, TextAlignment.Left);

                double jj = DinamikusAdatok.ViewModel.BelepesiPontSzelesseg - j1;
                jj = (DinamikusAdatok.ViewModel.BelepesiPontSzelesseg + jj) / 2;
                rect = new Rect(jj, 10 + (DinamikusAdatok.ViewModel.JatekosMagassag - (ma / 2)), sz, ma);
                drawingContext.DrawRoundedRectangle(Brushes.Red, null, rect, 15, 15);
                SzovegKirajzol(drawingContext, ">>>", rect.X, rect.Y, (int)sz / 3, Brushes.Black, TextAlignment.Left);

                rect = new Rect((DinamikusAdatok.ViewModel.BelepesiPontSzelesseg / 2) - (DinamikusAdatok.ViewModel.JatekosSzelesseg * 2 / 2), 10 + (DinamikusAdatok.ViewModel.JatekosMagassag * 2) + 10, sz, ma);
                drawingContext.DrawRoundedRectangle(Brushes.Red, null, rect, 15, 15);
                SzovegKirajzol(drawingContext, "Enter", rect.X, rect.Y, (int)sz / 3, Brushes.Black, TextAlignment.Left);
            }
        }

        private void SzornyekKirajzolasa(DrawingContext drawingContext)
        {
            foreach (Szorny item in this.Jatek.Szornyek)
            {
                item.Rect = new Rect(item.X, item.Y, item.Szelesseg, item.Magassag);
                Uri kepUri = null;
                BitmapImage kep = null;
                if (Uri.TryCreate(item.KepFajl, UriKind.RelativeOrAbsolute, out kepUri))
                {
                    kep = new BitmapImage(kepUri);
                }

                if (kep != null)
                {
                    drawingContext.DrawImage(kep, item.Rect);
                }
            }
        }

        private void LovesekKirajzolasa(DrawingContext drawingContext)
        {
            foreach (Loves item in this.Jatek.Lovesek)
            {
                item.Rect = new Rect(item.X, item.Y, item.Szelesseg, item.Magassag);
                Uri uri = null;
                BitmapImage kep = null;
                if (Uri.TryCreate(StatikusAdatok.LovesKepFajl, UriKind.RelativeOrAbsolute, out uri))
                {
                    kep = new BitmapImage(uri);
                }

                if (kep != null)
                {
                    drawingContext.DrawImage(kep, item.Rect);
                }
            }
        }

        private void KulonLegesMezokKirajzolasa(DrawingContext drawingContext)
        {
            foreach (KulonlegesMezo item in this.Jatek.KulonlegesMezok)
            {
                item.Rect = new Rect(item.X, item.Y, item.Szelesseg, item.Magassag);

                Uri uri = null;
                BitmapImage kep = null;
                if (Uri.TryCreate(item.KepFajl, UriKind.RelativeOrAbsolute, out uri))
                {
                    kep = new BitmapImage(uri);
                }

                if (kep != null)
                {
                    drawingContext.DrawImage(kep, item.Rect);
                }
            }
        }

        private void AkadalyokKirajzolasa(DrawingContext drawingContext)
        {
            foreach (Akadaly item in this.Jatek.Akadalyok)
            {
                item.Rect = new Rect(item.X, item.Y, item.Szelesseg, item.Magassag);

                drawingContext.DrawRectangle(System.Windows.Media.Brushes.Blue, new System.Windows.Media.Pen(System.Windows.Media.Brushes.Green, 2), item.Rect);
            }
        }

        private void JatekosKirajzolasa(DrawingContext drawingContext)
        {
            if (this.Jatek.Jatekos.NekimentValaminek)
            {
                this.Jatek.Jatekos.KepFajl = DinamikusAdatok.ViewModel.JatekosHalottKep;
            }
            else
            {
                this.Jatek.Jatekos.KepFajl = DinamikusAdatok.ViewModel.JatekosKepFile;
            }

            this.Jatek.Jatekos.Rect = new Rect(this.Jatek.Jatekos.X, this.Jatek.Jatekos.Y, this.Jatek.Jatekos.Szelesseg, this.Jatek.Jatekos.Magassag);

            Uri uri = null;
            BitmapImage kep = null;
            if (Uri.TryCreate(this.Jatek.Jatekos.KepFajl, UriKind.RelativeOrAbsolute, out uri))
            {
                kep = new BitmapImage(uri);
            }

            if (kep != null)
            {
                drawingContext.DrawImage(kep, this.Jatek.Jatekos.Rect);
                if (DinamikusAdatok.ViewModel.JatekosTamadhato == false)
                {
                    drawingContext.DrawEllipse(null, new Pen(Brushes.White, 3), new Point(this.Jatek.Jatekos.X + (this.Jatek.Jatekos.Szelesseg / 2), this.Jatek.Jatekos.Y + (this.Jatek.Jatekos.Magassag / 2)), this.Jatek.Jatekos.Szelesseg * 2, this.Jatek.Jatekos.Szelesseg * 2);
                }
            }
        }

        private void HatterKirajzol(DrawingContext drawingContext)
        {
            string kepFajl = StatikusAdatok.JatekHatterKep;
            BitmapImage kep = null;
            Rect rect = new Rect(0, 0, this.ActualWidth, this.ActualHeight);
            Uri uri = null;
            if (Uri.TryCreate(kepFajl, UriKind.RelativeOrAbsolute, out uri))
            {
                Uri kepUri = uri;
                kep = new BitmapImage(kepUri);
            }

            if (kep != null)
            {
                drawingContext.DrawImage(kep, rect);
            }
        }

        private void Felfuggeszt()
        {
            if (this.Jatek.JatekVege)
            {
                DinamikusAdatok.ViewModel.JatekKepKockaEltolas = this.Jatek.EredetiKepKockaEltolas;
                this.Jatek = new Jatek();
                this.Jatek.Init();
                this.Idozito.Start();
            }
            else if (!this.Jatek.Felfuggesztett)
            {
                this.Idozito.Stop();
                this.Jatek.Idozito.Stop();
                this.Jatek.Felfuggesztett = true;
                DinamikusAdatok.ViewModel.KijeloltMenuElemSzama = 0;
                this.Jatek.Menuben = true;
            }
            else
            {
                this.Idozito.Start();
                this.Jatek.Idozito.Start();
                this.Jatek.Felfuggesztett = false;
            }
        }

        private void UjJatekInditasa()
        {
            DinamikusAdatok.ViewModel.JatekKepKockaEltolas = this.Jatek.EredetiKepKockaEltolas;
            this.Jatek = new Jatek();
            this.Jatek.Init();
            this.Idozito.Start();
            this.Jatek.Menuben = false;
            this.Jatek.UJJatek = false;
        }
    }
}
