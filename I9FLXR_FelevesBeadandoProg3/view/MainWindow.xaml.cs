﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace I9Flxr_FelevesBeadandoProg3.View
{
    using System.Windows;
    using I9Flxr_FelevesBeadandoProg3.BL;
    using I9Flxr_FelevesBeadandoProg3.Model;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private JatekFrameWorkElement jatekFrameWorkElement;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// Ablak létrehozása
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            this.jatekFrameWorkElement = new JatekFrameWorkElement();
            this.DataContext = DinamikusAdatok.ViewModel;

            this.Width = DinamikusAdatok.ViewModel.BelepesiPontSzelesseg;
            this.Height = DinamikusAdatok.ViewModel.BelepesiPontMagassag;
            this.jatekFrameWorkElement.Width = DinamikusAdatok.ViewModel.BelepesiPontSzelesseg;
            this.jatekFrameWorkElement.Height = DinamikusAdatok.ViewModel.BelepesiPontMagassag;
            this.jatekFrameWorkElement.Jatek = new Jatek();
            this.jatekFrameWorkElement.Init();

            this.Content = this.jatekFrameWorkElement;
            this.MinHeight = DinamikusAdatok.ViewModel.BelepesiPontMagassag;
            this.MinWidth = DinamikusAdatok.ViewModel.BelepesiPontSzelesseg;
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.Height = this.ActualWidth / 1.7;
            this.jatekFrameWorkElement.Height = this.jatekFrameWorkElement.ActualWidth / 1.7;
            double szorzoMagassag = this.ActualHeight / DinamikusAdatok.ViewModel.BelepesiPontMagassag;
            double szorzoSzelesseg = this.ActualWidth / DinamikusAdatok.ViewModel.BelepesiPontSzelesseg;

            this.jatekFrameWorkElement.Width *= szorzoSzelesseg;
            this.jatekFrameWorkElement.Height *= szorzoMagassag;
            this.jatekFrameWorkElement.Jatek.Jatekos.Szelesseg *= szorzoSzelesseg;
            this.jatekFrameWorkElement.Jatek.Jatekos.Magassag *= szorzoMagassag;
            this.jatekFrameWorkElement.Jatek.Jatekos.X *= szorzoSzelesseg;
            this.jatekFrameWorkElement.Jatek.Jatekos.Y *= szorzoMagassag;

            foreach (Akadaly item in this.jatekFrameWorkElement.Jatek.Akadalyok)
            {
                item.Szelesseg *= szorzoSzelesseg;
                item.Magassag *= szorzoMagassag;
                item.X *= szorzoSzelesseg;
                item.Y *= szorzoMagassag;
            }

            foreach (KulonlegesMezo item in this.jatekFrameWorkElement.Jatek.KulonlegesMezok)
            {
                item.Szelesseg *= szorzoSzelesseg;
                item.Magassag *= szorzoMagassag;
                item.X *= szorzoSzelesseg;
                item.Y *= szorzoMagassag;
            }

            foreach (Loves item in this.jatekFrameWorkElement.Jatek.Lovesek)
            {
                item.Szelesseg *= szorzoSzelesseg;
                item.Magassag *= szorzoMagassag;
                item.X *= szorzoSzelesseg;
                item.Y *= szorzoMagassag;
            }

            foreach (Szorny item in this.jatekFrameWorkElement.Jatek.Szornyek)
            {
                item.Szelesseg *= szorzoSzelesseg;
                item.Magassag *= szorzoMagassag;
                item.X *= szorzoSzelesseg;
                item.Y *= szorzoMagassag;
            }

            DinamikusAdatok.ViewModel.SzornyMagassag *= szorzoMagassag;
            DinamikusAdatok.ViewModel.SzornySzelesseg *= szorzoSzelesseg;
            DinamikusAdatok.ViewModel.AkadalyMagassag *= szorzoMagassag;
            DinamikusAdatok.ViewModel.AkadalySzelesseg *= szorzoSzelesseg;
            DinamikusAdatok.ViewModel.KulonlegesMezoMagassag *= szorzoMagassag;
            DinamikusAdatok.ViewModel.KulonlegesMezoSzelesseg *= szorzoSzelesseg;
            DinamikusAdatok.ViewModel.LovesMagassag *= szorzoMagassag;
            DinamikusAdatok.ViewModel.LovesSzelesseg *= szorzoSzelesseg;
            DinamikusAdatok.ViewModel.JatekosMagassag *= szorzoMagassag;
            DinamikusAdatok.ViewModel.JatekosSzelesseg *= szorzoSzelesseg;
            DinamikusAdatok.ViewModel.JatekosY *= szorzoMagassag;
            DinamikusAdatok.ViewModel.JatekosX *= szorzoSzelesseg;
            DinamikusAdatok.ViewModel.JatekosKepkockaEltolasUgrashor *= szorzoMagassag;
            DinamikusAdatok.ViewModel.BelepesiPontMagassag *= szorzoMagassag;
            DinamikusAdatok.ViewModel.BelepesiPontSzelesseg *= szorzoSzelesseg;
            DinamikusAdatok.ViewModel.JatekKepKockaEltolas *= szorzoSzelesseg;

            this.Content = this.jatekFrameWorkElement;
        }
    }
}
