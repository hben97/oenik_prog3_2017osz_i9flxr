﻿// <copyright file="Loves.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace I9Flxr_FelevesBeadandoProg3.Model
{
    using System.Threading;
    using System.Windows;

    /// <summary>
    /// Egy Loves osztály, leír egy lövést
    /// </summary>
    public class Loves : PalyaTartalom
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Loves"/> class.
        /// Létrehoz egy új lövést
        /// </summary>
        /// <param name="x">X koordináta, ahova létre kell hozni</param>
        /// <param name="y">Y koordináta, ahova létre kell hozni</param>
        /// <param name="lovesIrany">A lövés iránya</param>
        /// <param name="lovesMagassag">A lövés elem magassága</param>
        /// <param name="lovesSzelesseg">A lövés elem szélessége</param>
        public Loves(double x, double y, LovesIrany lovesIrany, double lovesMagassag, double lovesSzelesseg)
        {
            this.X = x;
            this.Y = y;
            this.PalyaTartalomTipus = StatikusAdatok.LovesTartalomTipus;
            this.Magassag = lovesMagassag;
            this.Szelesseg = lovesSzelesseg;
            this.LovesIranya = lovesIrany;
            this.Rect = new Rect(this.X, this.Y, this.Szelesseg, this.Magassag);

            Thread szal = new Thread(() =>
            {
                StatikusAdatok.LovesPlayer.PlaySync();
            });
            szal.Start();
        }

        /// <summary>
        /// Gets or sets : Megadja a lövés irányát
        /// </summary>
        public LovesIrany LovesIranya { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether : megadja, hogy törölni kell-e a lövést a pályáról
        /// </summary>
        public bool Torolni { get; set; }
    }
}
