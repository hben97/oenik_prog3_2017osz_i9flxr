﻿// <copyright file="Szorny.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace I9Flxr_FelevesBeadandoProg3.Model
{
    using System;
    using System.Windows;

    /// <summary>
    /// Leír egy szörny pályatartalmat
    /// </summary>
    public class Szorny : PalyaTartalom
    {
        private static Random random = new Random();

        /// <summary>
        /// Initializes a new instance of the <see cref="Szorny"/> class.
        /// Létrehoz egy új Szorny objektumot
        /// </summary>
        /// <param name="palyaSzelesseg">A pálya szélessége, amin a játék folyik</param>
        /// <param name="palyaMagassag">A pálya magassága, amin a játék folyik</param>
        /// <param name="szornyMagassag">A szörny magassága, amivel létre kell hozni</param>
        /// <param name="szornySzelesseg">A szörny szélessége, amivel létre kell hozni</param>
        /// <param name="jatekosY">A játékban lévő játékos alapértelmezett Y koordinátája</param>
        /// <param name="jatekosMagassag">A játékban lévő játékos magassága</param>
        public Szorny(double palyaSzelesseg, double palyaMagassag, double szornyMagassag, double szornySzelesseg, double jatekosY, double jatekosMagassag)
        {
            this.PalyaTartalomTipus = StatikusAdatok.SzornyTartalomTipus;
            this.X = random.Next((int)palyaSzelesseg + 100, (int)palyaSzelesseg + 110);
            this.Magassag = szornyMagassag;
            this.Szelesseg = szornySzelesseg;
            this.Y = random.Next((int)palyaMagassag / 2, (int)(jatekosY + jatekosMagassag - this.Magassag));
            this.EletAmitLevonHANekiMennek = 1;
            this.Rect = new Rect(this.X, this.Y, this.Szelesseg, this.Magassag);
            int tip = random.Next(6);
            this.SzornyTipus = (SzornyTipus)tip;
            switch (this.SzornyTipus)
            {
                case SzornyTipus.KetEletu:
                    this.Elet = 2;
                    this.TudLoni = false;
                    break;
                case SzornyTipus.HaromEletu:
                    this.Elet = 3;
                    this.TudLoni = false;
                    break;
                case SzornyTipus.KetEletuLovo:
                    this.Elet = 2;
                    this.TudLoni = true;
                    break;
                case SzornyTipus.HaromEletulovo:
                    this.Elet = 3;
                    this.TudLoni = true;
                    break;
                case SzornyTipus.EgyeletuLovo:
                    this.Elet = 1;
                    this.TudLoni = true;
                    break;
                case SzornyTipus.EgyEletu:
                    this.Elet = 1;
                    this.TudLoni = false;
                    break;
                default:
                    this.Elet = 1;
                    this.TudLoni = false;
                    break;
            }

            if (this.TudLoni)
            {
                this.KepFajl = StatikusAdatok.LovoSzornyKepFile;
            }
            else
            {
                this.KepFajl = StatikusAdatok.SzornyKepFile;
            }
        }

        /// <summary>
        /// Gets or sets : megadj a szörny életét
        /// </summary>
        public int Elet { get; set; }

        /// <summary>
        /// Gets or sets : megadja, hogyha nekimegyünk hány életet kell levonni a játékostól
        /// </summary>
        public int EletAmitLevonHANekiMennek { get; set; }

        /// <summary>
        /// Gets or sets : megadja, hogy milyen típusú szörnyről van szó
        /// </summary>
        public SzornyTipus SzornyTipus { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether : megadja, hogy a szörny tud-e lőni
        /// </summary>
        public bool TudLoni { get; set; }

        /// <summary>
        /// Gets or sets : megadja a szörny képfájlának az elérési útvonalát
        /// </summary>
        public string KepFajl { get; set; }
    }
}
