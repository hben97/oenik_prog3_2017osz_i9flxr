﻿// <copyright file="Akadaly.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace I9Flxr_FelevesBeadandoProg3.Model
{
    using System;

    /// <summary>
    /// Egy osztály a pályán megjelenő akadályelemeknek
    /// </summary>
    public class Akadaly : PalyaTartalom
    {
        private int eletAmitLEvonHaNekiMennek;
        private Random random;

        /// <summary>
        /// Initializes a new instance of the <see cref="Akadaly"/> class.
        /// Létrehoz egy új akadály elemet
        /// </summary>
        /// <param name="palyaSzelesseg">A pálya teljes szélessége, amin dolgozunk</param>
        /// <param name="palyaMagassag">A pálya teljes magassága, amin játszunk</param>
        /// <param name="akadalyMaxMagassag">A létrejövő akadáy maximum megengedett magassága</param>
        /// <param name="jatekosY">A játékban lévő játékos alapértelmezett Y koordinátája, hogy kiszámolhasuk hol van a páya alja</param>
        /// <param name="akadalySzelesseg">A létrejövő akadály szélessége</param>
        /// <param name="jatekosMagassag">A játékban lévő játékos magassága</param>
        public Akadaly(double palyaSzelesseg, double palyaMagassag, double akadalyMaxMagassag, double jatekosY, double akadalySzelesseg, double jatekosMagassag)
        {
            this.random = new Random();
            this.EletAmitLevonHANekiMennek = this.random.Next(0, StatikusAdatok.AkadalyEletAmitElveszHANekiMegyunkMaximum + 1);
            this.PalyaTartalomTipus = StatikusAdatok.AkadalyPalyaTartalomTipus;
            this.X = this.random.Next((int)palyaSzelesseg + 100, (int)palyaSzelesseg + 110);

            this.Magassag = this.random.Next(80, (int)akadalyMaxMagassag);

            this.Szelesseg = akadalySzelesseg;
            int fentLent = this.random.Next(2);
            if (fentLent == 0)
            {
                this.Y = jatekosY + jatekosMagassag - this.Magassag; // pont a képernyő alja
            }
            else
            {
                this.Y = palyaMagassag / 3;
            }

            this.Rect = new System.Windows.Rect(this.X, this.Y, this.Szelesseg, this.Magassag);
        }

        /// <summary>
        /// Gets or sets : megadja, hogy mennyi életet vonjon le a játékostól, ha nekimennek
        /// </summary>
        public int EletAmitLevonHANekiMennek
        {
            get { return this.eletAmitLEvonHaNekiMennek; }
            set { this.eletAmitLEvonHaNekiMennek = value; }
        }
    }
}
