﻿// <copyright file="StatikusAdatok.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace I9Flxr_FelevesBeadandoProg3.Model
{
    using System;
    using System.Collections.ObjectModel;
    using System.Media;

    /// <summary>
    /// Tartalmaz minden statikus adatot, ami kell a játék inicializálásához és futásához
    /// </summary>
    public static class StatikusAdatok
    {
       private static TimeSpan jatekFrameWorkElementIdozitoSpan = new TimeSpan(0, 0, 0, 0, 1);
       private static TimeSpan jatekosIdozitoUgrashoz = new TimeSpan(0, 0, 0, 0, 1);
       private static ObservableCollection<string> jatekosKepFileLista = new ObservableCollection<string>() { "Resources\\jatekosAlap.png", "Resources\\playerHalott.png", "Resources\\player2.png", "Resources\\player2halott.png" }; // n.elem a sima játékos kép, n+1.elem a halott játékos kép
       private static PalyaTartalomTipus jatekosPalyaTartalomTipus = PalyaTartalomTipus.Jatekos;
       private static PalyaTartalomTipus lovesTartalomTipus = PalyaTartalomTipus.Loves;
       private static PalyaTartalomTipus kulonlegesMezoTartalomTipus = PalyaTartalomTipus.KulonlegesMezo;
       private static string kulonlegesMezoKepFile = "Resources\\kMezoAlap.png";
       private static string kulonlegesMezoRosszKepFile = "Resources\\rossz.png";
       private static string kulonlegesMezoEletKep = "Resources\\heart.png";
       private static string kulonlegesMezoRandomKepFile = "Resources\\random.png";
       private static int akadalyEletAmitElveszHANekiMegyunkMaximum = 2;
       private static PalyaTartalomTipus akadalyPalyaTartalomTipus = PalyaTartalomTipus.Akadaly;
       private static int szornyElet = 4; // maximum
       private static string szornyKepFile = "Resources\\szornyAlap.png";
       private static string lovoSzornyKepFile = "Resources\\shooterMonster.png";
       private static PalyaTartalomTipus szornyTartalomTipus = PalyaTartalomTipus.Szorny;
       private static string jatekHatterKep = "Resources\\hatter.png";
       private static ObservableCollection<string> jatekMenuElemekUJJatek = new ObservableCollection<string>() { "Új játék indítása", "Beállítások", "Kilépés" }; // sorrend nem módosítható, max más megfogalmazás
       private static ObservableCollection<string> jatekMenuElemekPause = new ObservableCollection<string>() { "Folytatás", "Új játék indítása", "Beállítások", "Kilépés" }; // sorrend nem módosítható, max más megfogalmazás
       private static SoundPlayer lovesPlayer = new SoundPlayer("Resources\\laser.wav");
       private static string lovesKepFajl = "Resources\\lovesAlap.png";

        /// <summary>
        /// Gets : megadja az időzítő intervallumát a játékmegjelenítéshez
        /// </summary>
        public static TimeSpan JatekFrameWorkElementIdozitoSpan
        {
            get { return jatekFrameWorkElementIdozitoSpan; }
        }

        /// <summary>
        /// Gets : megadja az időzítő intervallumát a játékos ugrásához
        /// </summary>
        public static TimeSpan JatekosIdozitoUgrashoz
        {
            get { return jatekosIdozitoUgrashoz; }
        }

        /// <summary>
        /// Gets : tartalmazza a képfájlokat a különböző játékosokhoz. N. elem a rendes képfájl, N+1.elem pedig a Nekimentvalaminek (sérülés) esetén van
        /// </summary>
        public static ObservableCollection<string> JatekosKepFileLista
        {
            get { return jatekosKepFileLista; }
        } // n.elem a sima játékos kép, n+1.elem a halott játékos kép

        /// <summary>
        /// Gets : megadja a pályatartalomtípusát a játékosnak
        /// </summary>
        public static PalyaTartalomTipus JatekosPalyaTartalomTipus
        {
            get { return jatekosPalyaTartalomTipus; }
        }

        /// <summary>
        /// Gets : megadja  Loves objektum pályatartalomtípusát
        /// </summary>
        public static PalyaTartalomTipus LovesTartalomTipus
        {
            get { return lovesTartalomTipus; }
        }

        /// <summary>
        /// Gets : megadja a KulonlegesMezo pályatartalomtípusát
        /// </summary>
        public static PalyaTartalomTipus KulonlegesMezoTartalomTipus
        {
            get { return kulonlegesMezoTartalomTipus; }
        }

        /// <summary>
        /// Gets : megadj a KulonlegesMezp képjájljának elérési útvonalát
        /// </summary>
        public static string KulonlegesMezoKepFile
        {
            get { return kulonlegesMezoKepFile; }
        }

        /// <summary>
        /// Gets : megadja a játékosra veszélyes KulonlegesMezo-k képfájljának elérési útvonalát
        /// </summary>
        public static string KulonlegesMezoRosszKepFile
        {
            get { return kulonlegesMezoRosszKepFile; }
        }

        /// <summary>
        /// Gets : megadja a KulonlegesMezo azon képfájljának elérési útvonalát, ami életet ad a játékosnak
        /// </summary>
        public static string KulonlegesMezoEletKep
        {
            get { return kulonlegesMezoEletKep; }
        }

        /// <summary>
        /// Gets : megadja  a KulonlegesMezo azon képfájlját, ami a random mezőkre vonatkozik
        /// </summary>
        public static string KulonlegesMezoRandomKepFile
        {
            get { return kulonlegesMezoRandomKepFile; }
        }

        /// <summary>
        /// Gets : megadja azt a maximum számot, amit egy akadály levonhat a játékos életéből, ha nekimegy
        /// </summary>
        public static int AkadalyEletAmitElveszHANekiMegyunkMaximum
        {
            get { return akadalyEletAmitElveszHANekiMegyunkMaximum; }
        }

        /// <summary>
        /// Gets : megadj az Akadaly pályatartalomtípusát
        /// </summary>
        public static PalyaTartalomTipus AkadalyPalyaTartalomTipus
        {
            get { return akadalyPalyaTartalomTipus; }
        }

        /// <summary>
        /// Gets : megadja a Szorny pályatartalom maximális életét, ami lehet neki
        /// </summary>
        public static int SzornyElet
        {
            get { return szornyElet; }
        } // maximum

        /// <summary>
        /// Gets : megadj a Szorny pályatartalom képfájljának elérési útvonalát
        /// </summary>
        public static string SzornyKepFile
        {
            get { return szornyKepFile; }
        }

        /// <summary>
        /// Gets : megadja egy lövésre képes Szorny képjájljának elérsi útvonalát
        /// </summary>
        public static string LovoSzornyKepFile
        {
            get { return lovoSzornyKepFile; }
        }

        /// <summary>
        /// Gets : megadja egy Szorny pályatartalomtrípusát
        /// </summary>
        public static PalyaTartalomTipus SzornyTartalomTipus
        {
            get { return szornyTartalomTipus; }
        }

        /// <summary>
        /// Gets: megadj a ajáték háttérképének elérési útvonalát
        /// </summary>
        public static string JatekHatterKep
        {
            get { return jatekHatterKep; }
        }

        /// <summary>
        /// Gets : Tartalmazza azokat a menüelemeket, amit új játék során kell megjeleníteni
        /// </summary>
        public static ObservableCollection<string> JatekMenuElemekUJJatek
        {
            get { return jatekMenuElemekUJJatek; }
        } // sorrend nem módosítható, max más megfogalmazás

        /// <summary>
        /// Gets : tartalmazza azokat a menüelemeket, amit a játék felfüggesztése során kell megjeleníteni
        /// </summary>
        public static ObservableCollection<string> JatekMenuElemekPause
        {
            get { return jatekMenuElemekPause; }
        } // sorrend nem módosítható, max más megfogalmazás

        /// <summary>
        /// Gets : a lejátszó a Szornyek lövéséhez
        /// </summary>
        public static SoundPlayer LovesPlayer
        {
            get { return lovesPlayer; }
        }

        /// <summary>
        /// Gets :  A Loves képfájljának elérési útvonala
        /// </summary>
        public static string LovesKepFajl
        {
            get { return lovesKepFajl; }
        }
    }
}
