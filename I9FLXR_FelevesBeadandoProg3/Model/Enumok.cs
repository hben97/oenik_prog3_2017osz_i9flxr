﻿// <copyright file="Enumok.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace I9Flxr_FelevesBeadandoProg3.Model
{
    /// <summary>
    /// Felsorolja a pályán megjelenő pályatípusokat
    /// </summary>
    public enum PalyaTartalomTipus
    {
        /// <summary>
        /// Egy Akadaly elem
        /// </summary>
        Akadaly,

        /// <summary>
        /// Egy KulonlegesMezo elem
        /// </summary>
        KulonlegesMezo,

        /// <summary>
        /// Egy Szorny elem
        /// </summary>
        Szorny,

        /// <summary>
        /// Egy Jatekos elem
        /// </summary>
        Jatekos,

        /// <summary>
        /// Egy Loves elem
        /// </summary>
        Loves
    }

    /// <summary>
    /// Megadja a létező szörnytípusokat
    /// </summary>
    public enum SzornyTipus
    {
        /// <summary>
        /// Kétéletű szörny
        /// </summary>
        KetEletu,

        /// <summary>
        /// Hároméletű szörny
        /// </summary>
        HaromEletu,

        /// <summary>
        /// Két életű lőni tudó szörny
        /// </summary>
        KetEletuLovo,

        /// <summary>
        /// Három életű lőni tudó szörny
        /// </summary>
        HaromEletulovo,

        /// <summary>
        /// Egy életű, lőni tudó szörny
        /// </summary>
        EgyeletuLovo,

        /// <summary>
        /// Egy életű szörny
        /// </summary>
        EgyEletu
    }

    /// <summary>
    /// Felsorolja a KulonlegesMezo típusait
    /// </summary>
    public enum KulonLegesMezoTipus
    {
        /// <summary>
        /// Pajzs
        /// </summary>
        OTMasodpercigNemVonLEletet,

        /// <summary>
        /// Játékgyorsító
        /// </summary>
        JatekGyorsitas,

        /// <summary>
        /// Játéklassító
        /// </summary>
        JatekLassitas,

        /// <summary>
        /// A pályán lévő szörnyek életét egyre beállító
        /// </summary>
        EgyLovesselGyilkolas,

        /// <summary>
        /// +1 életet adó
        /// </summary>
        PluszEgyElet,

        /// <summary>
        /// -1 életet elvevő
        /// </summary>
        MinuszEgyElet,

        /// <summary>
        /// +10 ontot adó
        /// </summary>
        PluszTizPont,

        /// <summary>
        /// -10 pontot elvevő
        /// </summary>
        MinuszTizPont
    }

    /// <summary>
    /// Megadja egy Loves elem irányát
    /// </summary>
    public enum LovesIrany
    {
        /// <summary>
        /// Balra lövés
        /// </summary>
        Balra,

        /// <summary>
        /// Jobbra lövés
        /// </summary>
        Jobbra
    }

    /// <summary>
    /// Megadja a sebességváltozás típusát
    /// </summary>
    public enum SebessegValtozas
    {
        /// <summary>
        /// Gyorsítás
        /// </summary>
        Gyorsabb,

        /// <summary>
        /// Lassítás
        /// </summary>
        Lassabb,

        /// <summary>
        /// Semmilyen sebességváltozás
        /// </summary>
        Eredeti
    }
}
