﻿// <copyright file="Jatekos.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace I9Flxr_FelevesBeadandoProg3.Model
{
    /// <summary>
    /// Leír egy játékos osztályt
    /// </summary>
    public class Jatekos : PalyaTartalom
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Jatekos"/> class.
        /// Új játékos létrehozása
        /// </summary>
        /// <param name="jatekosElet">Megadja a játékos életét</param>
        /// <param name="jatekosX">MEgadja a játékos alapértelmezett X koordinátáját</param>
        /// <param name="jatekosY">Megadja a játékos alapértelmezett Y koordinátáját</param>
        /// <param name="jatekosMagassag">Megadja a játékos magasságát</param>
        /// <param name="jatekosSzelesseg">Megadja a játékos szélességét</param>
        /// <param name="jatekosKepFile">Megadj a játékos képfájljának az elérési útvonalát</param>
        public Jatekos(int jatekosElet, double jatekosX, double jatekosY, double jatekosMagassag, double jatekosSzelesseg, string jatekosKepFile)
        {
            this.Ugrik = false;
            this.Ugorhat = true;
            this.Elet = jatekosElet;
            this.PalyaTartalomTipus = StatikusAdatok.JatekosPalyaTartalomTipus;
            this.X = jatekosX;
            this.Y = jatekosY;
            this.Magassag = jatekosMagassag;
            this.Szelesseg = jatekosSzelesseg;
            this.Pont = 0;
            this.NekimentValaminek = false;
            this.Rect = new System.Windows.Rect(this.X, this.Y, this.Szelesseg, this.Magassag);
            this.KepFajl = jatekosKepFile;
        }

        /// <summary>
        /// Gets or sets a value indicating whether : megadja, hogy a játékos jelen pillanatban ugrik-e vagy sem
        /// </summary>
        public bool Ugrik { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether : megadja, hogy a játékos jelen pillanatban ugorhat-e vagy sem
        /// </summary>
        public bool Ugorhat { get; set; }

        /// <summary>
        /// Gets or sets : megadja, hogy a játékosnak jelenleg hány pontja van
        /// </summary>
        public int Pont { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether : megadja, hogy a játékos neki ment-e valaminek
        /// </summary>
        public bool NekimentValaminek { get; set; }

        /// <summary>
        /// Gets or sets . megadja, hogy jelenleg hány élete van a játékosnak
        /// </summary>
        public int Elet { get; set; }

        /// <summary>
        /// Gets or sets :megadj a a játékos képfájljának az elérési útvonalát
        /// </summary>
        public string KepFajl { get; set; }
    }
}
