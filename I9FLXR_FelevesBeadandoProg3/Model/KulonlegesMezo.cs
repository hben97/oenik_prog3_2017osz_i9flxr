﻿// <copyright file="KulonlegesMezo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace I9Flxr_FelevesBeadandoProg3.Model
{
    using System;

    /// <summary>
    /// KulonlegesMezo osztály leírása
    /// </summary>
    public class KulonlegesMezo : PalyaTartalom
    {
        private static Random random = new Random();

        /// <summary>
        /// Initializes a new instance of the <see cref="KulonlegesMezo"/> class.
        /// Létrehoz egy új KulonlegesMezo-t
        /// </summary>
        /// <param name="palyaSzelesseg">A pálya szélessége, amin a játék folyik</param>
        /// <param name="palyaMagassag">A pálya magassága, amin a játék folyik</param>
        /// <param name="kulonlegesMezoMagassag">A létrejövő kulönlegesmező magassága</param>
        /// <param name="kulonlegesMezoSzelesseg">A létrejövő különlegesmező szélessége</param>
        /// <param name="jatekosY">A játékos alapértelmezett Y koordinátája</param>
        /// <param name="jatekosMagassag">A játékos alapértelmezett magassága</param>
        public KulonlegesMezo(double palyaSzelesseg, double palyaMagassag, double kulonlegesMezoMagassag, double kulonlegesMezoSzelesseg, double jatekosY, double jatekosMagassag)
        {
            this.X = random.Next((int)palyaSzelesseg + 100, (int)palyaSzelesseg + 110);

            this.Magassag = kulonlegesMezoMagassag;
            this.Szelesseg = kulonlegesMezoSzelesseg;

            this.PalyaTartalomTipus = StatikusAdatok.KulonlegesMezoTartalomTipus;
            int tip = random.Next(8);
            this.KulonLegesMezoTipus = (KulonLegesMezoTipus)tip;

            switch (this.KulonLegesMezoTipus)
            {
                case KulonLegesMezoTipus.OTMasodpercigNemVonLEletet:
                    this.KepFajl = StatikusAdatok.KulonlegesMezoRandomKepFile;
                    this.Szelesseg = this.Szelesseg * 2;
                    this.Magassag = this.Magassag * 2;
                    break;
                case KulonLegesMezoTipus.JatekGyorsitas:
                    this.KepFajl = StatikusAdatok.KulonlegesMezoRandomKepFile;
                    this.Szelesseg = this.Szelesseg * 2;
                    this.Magassag = this.Magassag * 2;
                    break;
                case KulonLegesMezoTipus.JatekLassitas:
                    this.KepFajl = StatikusAdatok.KulonlegesMezoRandomKepFile;
                    this.Szelesseg = this.Szelesseg * 2;
                    this.Magassag = this.Magassag * 2;
                    break;
                case KulonLegesMezoTipus.EgyLovesselGyilkolas:
                    this.KepFajl = StatikusAdatok.KulonlegesMezoRandomKepFile;
                    this.Szelesseg = this.Szelesseg * 2;
                    this.Magassag = this.Magassag * 2;
                    break;
                case KulonLegesMezoTipus.PluszEgyElet:
                    this.KepFajl = StatikusAdatok.KulonlegesMezoEletKep;
                    break;
                case KulonLegesMezoTipus.MinuszEgyElet:
                    this.KepFajl = StatikusAdatok.KulonlegesMezoRosszKepFile;
                    break;
                case KulonLegesMezoTipus.PluszTizPont:
                    this.KepFajl = StatikusAdatok.KulonlegesMezoKepFile;
                    break;
                case KulonLegesMezoTipus.MinuszTizPont:
                    this.KepFajl = StatikusAdatok.KulonlegesMezoRosszKepFile;
                    break;
                default:
                    this.KepFajl = StatikusAdatok.KulonlegesMezoRandomKepFile;
                    break;
            }

            this.Y = random.Next((int)(palyaMagassag * 0.25), (int)(jatekosY + jatekosMagassag - this.Magassag));

            this.Rect = new System.Windows.Rect(this.X, this.Y, this.Szelesseg, this.Magassag);
        }

        /// <summary>
        /// Gets or sets : megadja  a KulonlegesMezo típusát
        /// </summary>
        public KulonLegesMezoTipus KulonLegesMezoTipus { get; set; }

        /// <summary>
        /// Gets or sets : megadja a KulonlegesMezo képfájljának elérési útvonalát
        /// </summary>
        public string KepFajl { get; set; }
    }
}
