﻿// <copyright file="PalyaTartalom.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace I9Flxr_FelevesBeadandoProg3.Model
{
    using System.Windows;

    /// <summary>
    /// Leír egy pályatartalmat
    /// </summary>
    public abstract class PalyaTartalom
    {
        private double x;
        private double y;
        private double magassag;
        private double szelesseg;
        private Rect rect;
        private PalyaTartalomTipus palyaTartalomTipus;

        /// <summary>
        /// Gets or sets : megadja a pályatartalom X koordinátáját
        /// </summary>
        public double X
        {
            get { return this.x; }
            set { this.x = value; }
        }

        /// <summary>
        /// Gets or sets : megadja a pályatartalom y koordinátáját
        /// </summary>
        public double Y
        {
            get { return this.y; }
            set { this.y = value; }
        }

        /// <summary>
        /// Gets or sets : megadja a pályatartalom magasságát
        /// </summary>
        public double Magassag
        {
            get { return this.magassag; }
            set { this.magassag = value; }
        }

        /// <summary>
        /// Gets or sets : megadja a pályatartalom szélességét
        /// </summary>
        public double Szelesseg
        {
            get { return this.szelesseg; }
            set { this.szelesseg = value; }
        }

        /// <summary>
        /// Gets or sets : megadja a pályatartalomhoz tartozó téglalapot
        /// </summary>
        public Rect Rect
        {
            get { return this.rect; }
            set { this.rect = value; }
        }

        /// <summary>
        /// Gets or sets : megadja  a pályatartalom típusát
        /// </summary>
        public PalyaTartalomTipus PalyaTartalomTipus
        {
            get { return this.palyaTartalomTipus; }
            set { this.palyaTartalomTipus = value; }
        }
    }
}
