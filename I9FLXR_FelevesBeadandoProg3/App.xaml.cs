﻿// <copyright file="App.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace I9Flxr_FelevesBeadandoProg3
{
    using System.Windows;
    using I9Flxr_FelevesBeadandoProg3.BL;
    using I9Flxr_FelevesBeadandoProg3.Model;

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
    }
}
