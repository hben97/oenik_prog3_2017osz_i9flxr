﻿// <copyright file="Jatek.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace I9Flxr_FelevesBeadandoProg3.BL
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Threading;
    using I9Flxr_FelevesBeadandoProg3.Model;

    /// <summary>
    /// A teljes játék osztály, ami magába foglalja a játék logikáját
    /// </summary>
    public class Jatek
    {
        private ObservableCollection<Akadaly> akadalyok;
        private ObservableCollection<Loves> lovesek;
        private ObservableCollection<KulonlegesMezo> kulonlegesMezok;
        private ObservableCollection<Szorny> szornyek;
        private bool jatekVege;
        private Random random;
        private bool sebessegValtozas;
        private DispatcherTimer sebessegValtozasTimer;
        private int haladasSzam;
        private double eredetiKepKockaEltolasUgrashoz;
        private int eredetiHaladasiSzam;
        private int utolosoSzamSzam;
        private bool menuben;
        private bool ujJatek;

        /// <summary>
        /// Gets or sets : Az az időzító, ami segít a különleges képességek(pl. pajzs) végrehajtásához, illetve az utolsó megjelenített szöveg törlésére
        /// </summary>
        public DispatcherTimer Idozito { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether: megadja, hogy a játék jelenleg fel van-e függesztve
        /// </summary>
        public bool Felfuggesztett { get; set; }

        /// <summary>
        /// Gets or sets : Az eredeti képkockaeltolás a játékhoz, ha esetleg módosítanánk játék közben
        /// </summary>
        public double EredetiKepKockaEltolas { get; set; }

        /// <summary>
        /// Gets : meghatározza a sebesség változásának a fajtáját, mikor módosítunk a sebességen
        /// </summary>
        public SebessegValtozas SebessegValtozasFajta { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether : megadja, hogy vége van-e a játéknak. Igaz-ra állítás esetén hangot játszik le
        /// </summary>
        public bool JatekVege
        {
            get
            {
                return this.jatekVege;
            }

            set
            {
                this.jatekVege = value;
                if (value == true)
                {
                    System.Media.SystemSounds.Hand.Play();
                }
            }
        }

        /// <summary>
        /// Gets or sets : A játékosunk
        /// </summary>
        public Jatekos Jatekos { get; set; }

        /// <summary>
        /// Gets or sets : Az Akadaly elemeket tartalmazó Lista
        /// </summary>
        public ObservableCollection<Akadaly> Akadalyok
        {
            get
            {
                return this.akadalyok;
            }

            set
            {
                this.akadalyok = value;
            }
        }

        /// <summary>
        /// Gets or sets : A Loves elemeket tartalmazó lista
        /// </summary>
        public ObservableCollection<Loves> Lovesek
        {
            get
            {
                return this.lovesek;
            }

            set
            {
                this.lovesek = value;
            }
        }

        /// <summary>
        /// Gets or sets : A KulonlegesMezo elemeket tartalmazó lista
        /// </summary>
        public ObservableCollection<KulonlegesMezo> KulonlegesMezok
        {
            get
            {
                return this.kulonlegesMezok;
            }

            set
            {
                this.kulonlegesMezok = value;
            }
        }

        /// <summary>
        /// Gets or sets : A Szornyek-et tartalmazó lista
        /// </summary>
        public ObservableCollection<Szorny> Szornyek
        {
            get
            {
                return this.szornyek;
            }

            set
            {
                this.szornyek = value;
            }
        }

        /// <summary>
        /// Gets or sets : Az éppen aktuális utolsó megjelenítendő szöveg, amit a User tudtára akarunk adni (pl. +1 élet)
        /// </summary>
        public string UtolsoSzoveg { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether : megadja, hogy a menüben vagyunk e jelenleg
        /// </summary>
        public bool Menuben
        {
            get
            {
                return this.menuben;
            }

            set
            {
                this.menuben = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether : megadja, hogy a Beállításokban vagyunk e jelenleg
        /// </summary>
        public bool Beallitasokban { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether : megadja, hogy jelenleg egy új játékot kezdtünk e, vagy sem
        /// </summary>
        public bool UJJatek
        {
            get
            {
                return this.ujJatek;
            }

            set
            {
                this.ujJatek = value;
            }
        }

        /// <summary>
        /// Beaállítja az alapértelmezett értékeket a játékhoz
        /// </summary>
        public void Init()
        {
            this.JatekVege = false;
            this.Beallitasokban = false;
            this.random = new Random();
            this.Idozito = new DispatcherTimer();
            this.Felfuggesztett = false;
            this.sebessegValtozas = false;
            this.haladasSzam = 0;
            this.EredetiKepKockaEltolas = DinamikusAdatok.ViewModel.JatekKepKockaEltolas;
            this.eredetiKepKockaEltolasUgrashoz = DinamikusAdatok.ViewModel.JatekosKepkockaEltolasUgrashor;

            this.eredetiHaladasiSzam = DinamikusAdatok.ViewModel.JatekEredetiHaladasiSzam;
            this.utolosoSzamSzam = 0;
            this.menuben = true;
            this.ujJatek = true;
            this.SebessegValtozasFajta = SebessegValtozas.Eredeti;
            this.Akadalyok = new ObservableCollection<Akadaly>();
            this.Lovesek = new ObservableCollection<Loves>();
            this.KulonlegesMezok = new ObservableCollection<KulonlegesMezo>();
            this.Szornyek = new ObservableCollection<Szorny>();
            this.Jatekos = new Jatekos(DinamikusAdatok.ViewModel.JatekosElet, DinamikusAdatok.ViewModel.JatekosX, DinamikusAdatok.ViewModel.JatekosY, DinamikusAdatok.ViewModel.JatekosMagassag, DinamikusAdatok.ViewModel.JatekosSzelesseg, DinamikusAdatok.ViewModel.JatekosKepFile);
            DinamikusAdatok.ViewModel.JatekosTamadhato = true;
            DinamikusAdatok.ViewModel.JatekHaladasiSzam = DinamikusAdatok.ViewModel.JatekEredetiHaladasiSzam;

            this.Idozito.Interval = new TimeSpan(0, 0, 15);
            this.Idozito.Tick += (o, e) => // 15 másodpercenként ad 15 pontot,illetve "randomizálja" a pajzs meglétének az idejét
            {
                this.Jatekos.Pont += 15;

                DinamikusAdatok.ViewModel.JatekosTamadhato = true;
                this.Idozito.Stop();
            };

            this.sebessegValtozasTimer = new DispatcherTimer();
            this.sebessegValtozasTimer.Interval = new TimeSpan(0, 0, 3); // 3mp-es időzítés
            this.sebessegValtozasTimer.Tick += (o, e) =>
            {
                // emiatt a 3 mp-ből 6mp lesz!
                if (!this.sebessegValtozas)
                {
                    this.sebessegValtozas = true;
                }
                else
                {
                    // a swich a további funkciók miatt van
                    switch (this.SebessegValtozasFajta)
                    {
                        case SebessegValtozas.Gyorsabb: // ha gyorsítás volt
                            DinamikusAdatok.ViewModel.JatekKepKockaEltolas = this.EredetiKepKockaEltolas;
                            DinamikusAdatok.ViewModel.JatekosKepkockaEltolasUgrashor = this.eredetiKepKockaEltolasUgrashoz;
                            break;
                        case SebessegValtozas.Lassabb: // ha pedig lassítás
                            DinamikusAdatok.ViewModel.JatekKepKockaEltolas = this.EredetiKepKockaEltolas;
                            DinamikusAdatok.ViewModel.JatekosKepkockaEltolasUgrashor = this.eredetiKepKockaEltolasUgrashoz;

                            break;
                        default:
                            break;
                    }

                    this.sebessegValtozas = false; // fontos!
                    this.SebessegValtozasFajta = SebessegValtozas.Eredeti; // fontos!
                    this.sebessegValtozasTimer.Stop(); // megállítjuk, hogy ne menjen tovább a timer

                    DinamikusAdatok.ViewModel.JatekHaladasiSzam = this.eredetiHaladasiSzam; // visszaállítjuk a random generáláshoz
                    this.haladasSzam = 0; // ha esetleg már átléptük volna, akkor inkább állítsuk vissza 0-ra, a komplikációk miatt!
                }
            };
        }

        /// <summary>
        /// Bejárunk minden elemet, és végrehajtjuk a szükséges szabályokat a játékban
        /// </summary>
        public void Halad()
        {
            // ha nem vagyunk a menüben, csak akkor haladunk
            if (!this.Menuben)
            {
                // az aktuális kiírást (pl. +5 pont szöveg) egy számláló segítségével írjuk ki egyszerűen
                if (this.utolosoSzamSzam == 200)
                {
                    this.utolosoSzamSzam = 0; // visszaállítjuk a kezdő értékre
                    this.UtolsoSzoveg = string.Empty; // eltüntetjük a szöveget
                }
                else
                {
                    this.utolosoSzamSzam++; // különben csak növeljük egyszerűen
                }

                // ilyenkor generálunk új pályatartalmat
                if (this.haladasSzam == DinamikusAdatok.ViewModel.JatekHaladasiSzam)
                {
                    this.UjRandomGeneralasa();
                    this.haladasSzam = 0; // hasonlóan, mint az előző számlálónál
                    this.Jatekos.NekimentValaminek = false; // miután nekimentünk valaminek ez a változó true lesz, így állítsuk vissza (megjelenítésnél lesz lényege, hogy csak rövid ideig legyen piros a játékos, ha pl. nekiment valaminek)
                }
                else
                {
                    this.haladasSzam++;
                }

                if (this.JatekVege)
                {
                    this.Idozito.Stop();
                }

                ObservableCollection<PalyaTartalom> torolni = new ObservableCollection<PalyaTartalom>();

                for (int i = 0; i < this.Akadalyok.Count; i++)
                {
                    if (this.Akadalyok[i].X + this.Akadalyok[i].Szelesseg - DinamikusAdatok.ViewModel.JatekKepKockaEltolas < 0)
                    {
                        // ha kimentünk a játéktérből, akkor adjuk hozzá a törlendőkhöz
                        torolni.Add(this.Akadalyok[i]);
                    }
                    else
                    {
                        // ha bent vagyunk még a játéktérben, akkor csak tegyük arréb
                        this.Akadalyok[i].X -= DinamikusAdatok.ViewModel.JatekKepKockaEltolas;
                        this.SzabalyMegvalositas(this.Akadalyok[i]); // valósítsuk meg a játék szabályokat is
                    }
                }

                foreach (Akadaly item in torolni)
                {
                    this.Akadalyok.Remove(item); // törlünk mindnet, ami már nincs a játéktérben
                }

                torolni = new ObservableCollection<PalyaTartalom>();

                for (int i = 0; i < this.KulonlegesMezok.Count; i++)
                {
                    if (this.KulonlegesMezok[i].X + this.KulonlegesMezok[i].Szelesseg - DinamikusAdatok.ViewModel.JatekKepKockaEltolas < 0)
                    {
                        torolni.Add(this.KulonlegesMezok[i]);
                    }
                    else
                    {
                        this.KulonlegesMezok[i].X -= DinamikusAdatok.ViewModel.JatekKepKockaEltolas;
                        this.SzabalyMegvalositas(this.KulonlegesMezok[i]);
                    }
                }

                foreach (KulonlegesMezo item in torolni)
                {
                    this.KulonlegesMezok.Remove(item);
                }

                torolni = new ObservableCollection<PalyaTartalom>();

                for (int i = 0; i < this.Lovesek.Count; i++)
                {
                    if (this.Lovesek[i].X + this.Lovesek[i].Szelesseg - DinamikusAdatok.ViewModel.JatekKepKockaEltolas < 0 || this.Lovesek[i].Torolni)
                    {
                        torolni.Add(this.Lovesek[i]);
                    }
                    else
                    {
                        // lövésnél meg kell néznünk, hogy balra vagy jobbra megy (játékos vagy szörny lőtte-e)
                        switch (this.Lovesek[i].LovesIranya)
                        {
                            case LovesIrany.Balra:
                                this.Lovesek[i].X -= DinamikusAdatok.ViewModel.JatekKepKockaEltolas * 1.2; // ha szörny lőtte, akkor kicsit gyorsabb a lövés
                                this.Lovesek[i].Y = this.Lovesek[i].Y + 0.1; // egy kis "egyenletes gravitáció", hogy ne teljesen egyenesen menjen a lövés
                                break;
                            case LovesIrany.Jobbra:
                                this.Lovesek[i].X += DinamikusAdatok.ViewModel.JatekKepKockaEltolas;
                                this.Lovesek[i].Y = this.Lovesek[i].Y + 0.1;

                                if (this.Lovesek[i].X >= DinamikusAdatok.ViewModel.BelepesiPontSzelesseg)
                                {
                                    this.Lovesek[i].Torolni = true;
                                }

                                int seged = 0;

                                // megnézem a szörnyek életének összegét
                                for (int j = 0; j < this.Szornyek.Count; j++)
                                {
                                    seged += this.Szornyek[j].Elet;
                                }

                                this.LovesEgybeEsikESzornnyelSzabaly(this.Lovesek[i]); // megvalósítom a szabályt, azaz ha nekiment egy szörnynek, akkor csökkentem az életét a szörnynek

                                int seged2 = 0;

                                // újra megszámolom a szörnyek össz-életét
                                for (int j = 0; j < this.Szornyek.Count; j++)
                                {
                                    seged2 += this.Szornyek[j].Elet;
                                }

                                // ha kevesebb a szörnyek összélete, mint először számoltam, akkor kilőttem egy szörnyet, törlöm a lövést és kapok 5 pontot
                                if (seged2 < seged)
                                {
                                    torolni.Add(this.Lovesek[i]);
                                    this.Jatekos.Pont += 5;
                                    this.UtolsoSzoveg = "+5 pont"; // ki is iratom, hogy kaptam +5 pontot
                                }

                                break;
                            default:
                                this.Lovesek[i].X += DinamikusAdatok.ViewModel.JatekKepKockaEltolas;
                                break;
                        }

                        this.SzabalyMegvalositas(this.Lovesek[i]); // itt most a játékosra vizsgálom, hogy nekiment-e a lövés
                    }
                }

                foreach (Loves item in torolni)
                {
                    this.Lovesek.Remove(item);
                }

                torolni = new ObservableCollection<PalyaTartalom>();

                for (int i = 0; i < this.Szornyek.Count; i++)
                {
                    if (this.Szornyek[i].X + this.Szornyek[i].Szelesseg - DinamikusAdatok.ViewModel.JatekKepKockaEltolas < 0)
                    {
                        torolni.Add(this.Szornyek[i]);
                    }
                    else
                    {
                        this.Szornyek[i].X -= DinamikusAdatok.ViewModel.JatekKepKockaEltolas;
                        this.SzabalyMegvalositas(this.Szornyek[i]);

                        // ha a szörnyünk képes lőni, akkor egyszer lövünk vele
                        if ((this.Szornyek[i].TudLoni && this.haladasSzam == 0) || this.haladasSzam == 200)
                        {
                            Loves lovunk = new Loves(this.Szornyek[i].X, this.Szornyek[i].Y, LovesIrany.Balra, DinamikusAdatok.ViewModel.LovesMagassag, DinamikusAdatok.ViewModel.LovesSzelesseg);
                            this.Lovesek.Add(lovunk);
                            this.Szornyek[i].TudLoni = false; // többet nem akarom, hogy lőjön
                        }
                    }
                }

                foreach (Szorny item in torolni)
                {
                    this.Szornyek.Remove(item);
                }

                torolni = new ObservableCollection<PalyaTartalom>();
            }
        }

        private void LovesEgybeEsikESzornnyelSzabaly(Loves loves)
        {
            ObservableCollection<Szorny> torolni = new ObservableCollection<Szorny>();

            // ha a lövés eltalált egy szörnyet, akkor levonunk egyet a szörny életéből. Ha meghalt, töröljük is
            for (int i = 0; i < this.Szornyek.Count; i++)
            {
                bool metsziEgymast = loves.Rect.IntersectsWith(this.Szornyek[i].Rect);
                if (metsziEgymast)
                {
                    this.Szornyek[i].Elet--;
                    if (this.Szornyek[i].Elet <= 0)
                    {
                        torolni.Add(this.Szornyek[i]);
                    }
                }
            }

            foreach (Szorny item in torolni)
            {
                this.Szornyek.Remove(item);
            }

            torolni = new ObservableCollection<Szorny>();
        }

        private void SzabalyMegvalositas(PalyaTartalom tartalom)
        {
            // Megvizsgáljuk, hogy az objektum metszi e a játékosunkat
            bool metszikEgymast = tartalom.Rect.IntersectsWith(this.Jatekos.Rect);

            // ha metszik egymást, csak akkor csinálunk bármit is velük
            if (metszikEgymast)
            {
                bool eltuntet = true; // eltüntetjük majd az objektumot

                switch (tartalom.PalyaTartalomTipus)
                {
                    case PalyaTartalomTipus.Akadaly:
                        if (DinamikusAdatok.ViewModel.JatekosTamadhato)
                        {
                            Akadaly akadaly = tartalom as Akadaly;
                            if (akadaly.EletAmitLevonHANekiMennek > 0)
                            {
                                // ha az akadály von le életet,akkor levonjuk a játékostól
                                // kiírjuk, hogy levontunk X életet
                                // bekapcsoljuk, hogy a játékos nekiment valaminek
                                this.Jatekos.Elet -= akadaly.EletAmitLevonHANekiMennek;
                                this.UtolsoSzoveg = "-" + akadaly.EletAmitLevonHANekiMennek + " élet";
                                this.Jatekos.NekimentValaminek = true;
                            }
                        }

                        // ha meghaltunk
                        if (this.Jatekos.Elet <= 0)
                        {
                            this.JatekVege = true;
                        }

                        break;
                    case PalyaTartalomTipus.KulonlegesMezo:
                        switch ((tartalom as KulonlegesMezo).KulonLegesMezoTipus)
                        {
                            case KulonLegesMezoTipus.OTMasodpercigNemVonLEletet:
                                this.UtolsoSzoveg = "Pazsj BE";
                                DinamikusAdatok.ViewModel.JatekosTamadhato = false;
                                this.Idozito.Start(); // ez fogja majd kikapcsolni a pajzsot
                                break;

                            case KulonLegesMezoTipus.JatekGyorsitas:
                                this.UtolsoSzoveg = "X 1.5 seesség";
                                DinamikusAdatok.ViewModel.JatekosKepkockaEltolasUgrashor = this.eredetiKepKockaEltolasUgrashoz * 1.5; // ugrás is arányos legyen
                                DinamikusAdatok.ViewModel.JatekKepKockaEltolas = this.EredetiKepKockaEltolas * 1.5; // képkockaeltolás is arányos legyen
                                this.SebessegValtozasFajta = SebessegValtozas.Gyorsabb;
                                this.sebessegValtozasTimer.Start();
                                DinamikusAdatok.ViewModel.JatekHaladasiSzam = (int)Math.Round(DinamikusAdatok.ViewModel.JatekHaladasiSzam / 1.5); // ritkábban generáljon új random pályatartalmat, azért fontos!
                                if (this.haladasSzam > DinamikusAdatok.ViewModel.JatekHaladasiSzam)
                                {
                                    this.haladasSzam = DinamikusAdatok.ViewModel.JatekHaladasiSzam - 35; // ha meghaladtuk volna az új számot, akkor azért csökkentsük le, hogy ne fusson örökké
                                }

                                break;

                            case KulonLegesMezoTipus.JatekLassitas:
                                this.UtolsoSzoveg = "X 0.5 seesség";
                                DinamikusAdatok.ViewModel.JatekosKepkockaEltolasUgrashor = this.eredetiKepKockaEltolasUgrashoz * 0.5;
                                DinamikusAdatok.ViewModel.JatekKepKockaEltolas = this.EredetiKepKockaEltolas * 0.5;
                                this.SebessegValtozasFajta = SebessegValtozas.Lassabb;
                                this.sebessegValtozasTimer.Start();
                                DinamikusAdatok.ViewModel.JatekHaladasiSzam *= 2;
                                break;

                            case KulonLegesMezoTipus.EgyLovesselGyilkolas:
                                this.UtolsoSzoveg = "Egy lövéssel gyilkolás";

                                // a már legenerált szörnyeket tudjuk majd csak egy lövéssel megölni
                                foreach (Szorny item in this.Szornyek)
                                {
                                    item.Elet = 1;
                                }

                                break;

                            case KulonLegesMezoTipus.PluszEgyElet:
                                this.UtolsoSzoveg = "+1 élet";
                                this.Jatekos.Elet++;
                                break;

                            case KulonLegesMezoTipus.MinuszEgyElet:
                                this.Jatekos.Elet--; // ha mező veszi el az életet, akkor nem védi meg a pajzs sem
                                this.UtolsoSzoveg = "-1 élet";
                                this.Jatekos.NekimentValaminek = true;
                                if (this.Jatekos.Elet <= 0)
                                {
                                    this.JatekVege = true;
                                }

                                break;

                            case KulonLegesMezoTipus.PluszTizPont:
                                this.UtolsoSzoveg = "+10 pont";
                                this.Jatekos.Pont += 10;
                                break;

                            case KulonLegesMezoTipus.MinuszTizPont:
                                this.UtolsoSzoveg = "-10 pont";
                                this.Jatekos.Pont -= 10;
                                this.Jatekos.NekimentValaminek = true;
                                break;

                            default:
                                break;
                        }

                        break;

                    case PalyaTartalomTipus.Szorny:
                        if (DinamikusAdatok.ViewModel.JatekosTamadhato && (tartalom as Szorny).EletAmitLevonHANekiMennek > 0)
                        {
                            this.Jatekos.Elet--;
                            this.UtolsoSzoveg = "-1 élet";
                            this.Jatekos.NekimentValaminek = true;
                        }

                        if (this.Jatekos.Elet <= 0)
                        {
                            this.JatekVege = true;
                        }

                        break;

                    case PalyaTartalomTipus.Loves:
                        if ((tartalom as Loves).LovesIranya == LovesIrany.Balra)
                        {
                            if (DinamikusAdatok.ViewModel.JatekosTamadhato)
                            {
                                this.Jatekos.Elet--;
                                this.UtolsoSzoveg = "-1 élet";
                                this.Jatekos.NekimentValaminek = true;
                            }

                            if (this.Jatekos.Elet <= 0)
                            {
                                this.JatekVege = true;
                            }
                        }
                        else
                        {
                            eltuntet = false; // ha mi lőttük ki, akkor ne tüntessük el!!
                        }

                        break;

                    default:
                        break;
                }

                // úgy a legegyszerűbb törölni az objektumot, hogy kirajuk a pályáról, így a következő Tick-re majd magától kitörlődik
                if (eltuntet)
                {
                    tartalom.Y = DinamikusAdatok.ViewModel.BelepesiPontMagassag * 2;
                    tartalom.X = 0;
                }
            }
        }

        private void UjRandomGeneralasa()
        {
            int uj = this.random.Next(3);
            switch (uj)
            {
                case 0:
                    this.Akadalyok.Add(new Akadaly(DinamikusAdatok.ViewModel.BelepesiPontSzelesseg, DinamikusAdatok.ViewModel.BelepesiPontMagassag, DinamikusAdatok.ViewModel.AkadalyMagassag, DinamikusAdatok.ViewModel.JatekosY, DinamikusAdatok.ViewModel.JatekosSzelesseg, DinamikusAdatok.ViewModel.JatekosMagassag));
                    break;
                case 1:
                    this.Szornyek.Add(new Szorny(DinamikusAdatok.ViewModel.BelepesiPontSzelesseg, DinamikusAdatok.ViewModel.BelepesiPontMagassag, DinamikusAdatok.ViewModel.SzornyMagassag, DinamikusAdatok.ViewModel.SzornySzelesseg, DinamikusAdatok.ViewModel.JatekosY, DinamikusAdatok.ViewModel.JatekosMagassag));
                    break;
                case 2:
                    this.KulonlegesMezok.Add(new KulonlegesMezo(DinamikusAdatok.ViewModel.BelepesiPontSzelesseg, DinamikusAdatok.ViewModel.BelepesiPontMagassag, DinamikusAdatok.ViewModel.KulonlegesMezoMagassag, DinamikusAdatok.ViewModel.KulonlegesMezoSzelesseg, DinamikusAdatok.ViewModel.JatekosY, DinamikusAdatok.ViewModel.JatekosMagassag));
                    break;
                default:
                    this.Akadalyok.Add(new Akadaly(DinamikusAdatok.ViewModel.BelepesiPontSzelesseg, DinamikusAdatok.ViewModel.BelepesiPontMagassag, DinamikusAdatok.ViewModel.AkadalyMagassag, DinamikusAdatok.ViewModel.JatekosY, DinamikusAdatok.ViewModel.JatekosSzelesseg, DinamikusAdatok.ViewModel.JatekosMagassag));
                    break;
            }
        }
    }
}
