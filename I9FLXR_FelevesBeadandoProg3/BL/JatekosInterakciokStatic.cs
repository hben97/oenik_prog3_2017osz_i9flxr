﻿// <copyright file="JatekosInterakciokStatic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace I9Flxr_FelevesBeadandoProg3.BL
{
    using System.Threading.Tasks;
    using I9Flxr_FelevesBeadandoProg3.Model;

    /// <summary>
    /// A játékos mozgásához egy statikus osztály
    /// </summary>
    public static class JatekosInterakciokStatic
     {
        /// <summary>
        /// A kapott játékos példányon hajt végre egy ugrást
        /// </summary>
        /// <param name="jatekos">Jatekos példány</param>
        public static async void Ugrik(Jatekos jatekos)
        {
            if (jatekos.Ugrik == false && jatekos.Ugorhat)
            {
                jatekos.Ugrik = true;
                double yon = jatekos.Y;
                double seged = jatekos.Magassag * 4; // ekkorát tudunk ugrani
                double vegPozicio = jatekos.Y - seged; // eddig megyünk el
                while (jatekos.Y > vegPozicio && jatekos.Ugorhat)
                {
                    jatekos.Y = jatekos.Y - DinamikusAdatok.ViewModel.JatekosKepkockaEltolasUgrashor;
                    await Task.Delay(StatikusAdatok.JatekosIdozitoUgrashoz);
                }

                while (jatekos.Y < yon && jatekos.Ugorhat)
                {
                    jatekos.Y = jatekos.Y + DinamikusAdatok.ViewModel.JatekosKepkockaEltolasUgrashor;
                    await Task.Delay(StatikusAdatok.JatekosIdozitoUgrashoz);
                }

                jatekos.Ugrik = false;
            }
        }

        /// <summary>
        /// A kapott Játékos pédányt a kezdő pozícióba helyezi
        /// </summary>
        /// <param name="jatekos">Kapott Jatekos példány</param>
        public static async void LeaFoldre(Jatekos jatekos)
        {
            jatekos.Ugorhat = false;

            while (jatekos.Y < DinamikusAdatok.ViewModel.JatekosY)
            {
                jatekos.Y = jatekos.Y + (DinamikusAdatok.ViewModel.JatekosKepkockaEltolasUgrashor * 1.5); // gyorsabban megyünk le
                await Task.Delay(StatikusAdatok.JatekosIdozitoUgrashoz);
            }

            jatekos.Y = DinamikusAdatok.ViewModel.JatekosY;

            jatekos.Ugorhat = true;
        }

        /// <summary>
        /// Létrehoz egy új Loves példányt a Jatekos helyzete alapján, majd ezt visszaadja
        /// </summary>
        /// <param name="jatekos">Jatekos példány, aminek a koordinátái alapján elhelyezzük az új Loves objektumunkat</param>
        /// <returns>Egy Loves példány</returns>
        public static Loves Loves(PalyaTartalom jatekos)
        {
            Loves loves = new Loves(jatekos.X + jatekos.Szelesseg + 3, jatekos.Y + (jatekos.Magassag * 0.75), LovesIrany.Jobbra, DinamikusAdatok.ViewModel.LovesMagassag, DinamikusAdatok.ViewModel.LovesSzelesseg);
            return loves;
        }
    }
}
