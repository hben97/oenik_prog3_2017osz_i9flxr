﻿// <copyright file="ViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace I9Flxr_FelevesBeadandoProg3.BL
{
    /// <summary>
    /// A játék során változó adatokat tároljuk itt, a könnyebb hozzáférhetőség miatt
    /// </summary>
    public class ViewModel
    {
        private double jatekKepKockaEltolas;
        private double belepesiPontSzelesseg;
        private double belepesiPontMagassag;
        private double jatekosKepkockaEltolasUgrashor;
        private string jatekosKepFile;
        private string jatekosHalottKep;
        private double jatekosX;
        private double jatekosSzelesseg;
        private double jatekosMagassag;
        private double jatekosY;
        private bool jatekosTamadhato;
        private int jatekosElet;
        private double lovesMagassag;
        private double lovesSzelesseg;
        private double kulonlegesMezoMagassag;
        private double kulonlegesMezoSzelesseg;
        private double akadalySzelesseg;
        private double akadalyMagassag;
        private double szornySzelesseg;
        private double szornyMagassag;
        private int jatekHaladasiSzam;
        private int jatekEredetiHaladasiSzam;
        private int kijeloltMenuElemSzama;

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModel"/> class.
        /// Alapértékeket adunk
        /// </summary>
        public ViewModel()
        {
            this.BelepesiPontSzelesseg = 680;
            this.BelepesiPontMagassag = 400;
            this.JatekosKepkockaEltolasUgrashor = 10;
            this.JatekosKepFile = "Resources\\jatekosAlap.png";
            this.JatekosHalottKep = "Resources\\playerHalott.png";
            this.JatekosX = 0;
            this.JatekosSzelesseg = 40;
            this.JatekosMagassag = 70;
            this.JatekosY = this.BelepesiPontMagassag - (this.JatekosMagassag * 1.87); // 290
            this.JatekosTamadhato = true;
            this.JatekosElet = 4;
            this.LovesMagassag = 3;
            this.LovesSzelesseg = 3;
            this.KulonlegesMezoMagassag = 15;
            this.KulonlegesMezoSzelesseg = 15;
            this.AkadalySzelesseg = this.JatekosSzelesseg;
            this.AkadalyMagassag = this.JatekosMagassag * 1.5;
            this.SzornySzelesseg = 30;
            this.SzornyMagassag = 30;
            this.JatekHaladasiSzam = 65;
            this.JatekEredetiHaladasiSzam = 65;
            this.JatekKepKockaEltolas = 5;
            this.KijeloltMenuElemSzama = 0;
        }

        /// <summary>
        /// Gets or sets : megadja, hogy a játék mennyivel tolja el az elemeket egy időzítő Tick-en belül
        /// </summary>
        public double JatekKepKockaEltolas
        {
            get
            {
                return this.jatekKepKockaEltolas;
            }

            set
            {
                this.jatekKepKockaEltolas = value;
            }
        }

        /// <summary>
        ///  Gets or sets : megadja, hogy jelenleg pontosan mekkora a pálya szélessége
        /// </summary>
        public double BelepesiPontSzelesseg
        {
            get
            {
                return this.belepesiPontSzelesseg;
            }

            set
            {
                this.belepesiPontSzelesseg = value;
            }
        }

        /// <summary>
        ///  Gets or sets : megadja, hogy pontosan mekkora a pálya magassága
        /// </summary>
        public double BelepesiPontMagassag
        {
            get
            {
                return this.belepesiPontMagassag;
            }

            set
            {
                this.belepesiPontMagassag = value;
            }
        }

        /// <summary>
        ///  Gets or sets : megadja, hogy milyen képkockaeltolást kell használni, miközben a játékos ugrik
        /// </summary>
        public double JatekosKepkockaEltolasUgrashor
        {
            get
            {
                return this.jatekosKepkockaEltolasUgrashor;
            }

            set
            {
                this.jatekosKepkockaEltolasUgrashor = value;
            }
        }

        /// <summary>
        ///  Gets or sets : megadja a játékos képfájljának az elérési útvonalát
        /// </summary>
        public string JatekosKepFile
        {
            get
            {
                return this.jatekosKepFile;
            }

            set
            {
                this.jatekosKepFile = value;
            }
        }

        /// <summary>
        ///  Gets or sets : megadja az elérési útvonalát annak a képnek, amikor a játékos nekimegy valaminek
        /// </summary>
        public string JatekosHalottKep
        {
            get
            {
                return this.jatekosHalottKep;
            }

            set
            {
                this.jatekosHalottKep = value;
            }
        }

        /// <summary>
        ///  Gets or sets : megadja a játékos alapértelmezett X koordinátáját
        /// </summary>
        public double JatekosX
        {
            get
            {
                return this.jatekosX;
            }

            set
            {
                this.jatekosX = value;
            }
        }

        /// <summary>
        ///  Gets or sets : megadja a játékos szélességét
        /// </summary>
        public double JatekosSzelesseg
        {
            get
            {
                return this.jatekosSzelesseg;
            }

            set
            {
                this.jatekosSzelesseg = value;
            }
        }

        /// <summary>
        ///  Gets or sets : megadja a játékos magasságát
        /// </summary>
        public double JatekosMagassag
        {
            get
            {
                return this.jatekosMagassag;
            }

            set
            {
                this.jatekosMagassag = value;
            }
        }

        /// <summary>
        ///  Gets or sets : megadja a játékos alapértelmezett Y koordinátáját
        /// </summary>
        public double JatekosY
        {
            get
            {
                return this.jatekosY;
            }

            set
            {
                this.jatekosY = value;
            }
        }

        /// <summary>
        ///  Gets or sets a value indicating whether : megadja, hogy a játékos jelenleg támadható-e
        /// </summary>
        public bool JatekosTamadhato
        {
            get
            {
                return this.jatekosTamadhato;
            }

            set
            {
                this.jatekosTamadhato = value;
            }
        }

        /// <summary>
        ///  Gets or sets : megadja, hogy jelenleg hány élete van a játékosnak
        /// </summary>
        public int JatekosElet
        {
            get
            {
                return this.jatekosElet;
            }

            set
            {
                this.jatekosElet = value;
            }
        }

        /// <summary>
        ///  Gets or sets : megadja, hogy mekkora legyen egy létrejövő Loves magassága
        /// </summary>
        public double LovesMagassag
        {
            get
            {
                return this.lovesMagassag;
            }

            set
            {
                this.lovesMagassag = value;
            }
        }

        /// <summary>
        ///  Gets or sets : megadja, hogy mekkora legyen egy létrejövő Loves szélessége
        /// </summary>
        public double LovesSzelesseg
        {
            get
            {
                return this.lovesSzelesseg;
            }

            set
            {
                this.lovesSzelesseg = value;
            }
        }

        /// <summary>
        ///  Gets or sets : megadja, hogy mekkora legyen egy létrejövő KulonlegesMezo magassága
        /// </summary>
        public double KulonlegesMezoMagassag
        {
            get
            {
                return this.kulonlegesMezoMagassag;
            }

            set
            {
                this.kulonlegesMezoMagassag = value;
            }
        }

        /// <summary>
        /// Gets or sets : megadja, hogy mekkora legyen egy létrejövő KulonlegesMezo szélessége
        /// </summary>
        public double KulonlegesMezoSzelesseg
        {
            get
            {
                return this.kulonlegesMezoSzelesseg;
            }

            set
            {
                this.kulonlegesMezoSzelesseg = value;
            }
        }

        /// <summary>
        /// Gets or sets : megadja, hogy mekkora legyen egy létrejövő Akadaly szélessége
        /// </summary>
        public double AkadalySzelesseg
        {
            get
            {
                return this.akadalySzelesseg;
            }

            set
            {
                this.akadalySzelesseg = value;
            }
        }

        /// <summary>
        /// Gets or sets : megadja, hogy mekkora legyen egy létrejövő Akadaly magassága
        /// </summary>
        public double AkadalyMagassag
        {
            get
            {
                return this.akadalyMagassag;
            }

            set
            {
                this.akadalyMagassag = value;
            }
        }

        /// <summary>
        /// Gets or sets : megadja, hogy mekkora legyen egy létrejövő Szorny szélessége
        /// </summary>
        public double SzornySzelesseg
        {
            get
            {
                return this.szornySzelesseg;
            }

            set
            {
                this.szornySzelesseg = value;
            }
        }

        /// <summary>
        /// Gets or sets : megadja, hogy mekkora legyen egy létrejövő Szorny magassága
        /// </summary>
        public double SzornyMagassag
        {
            get
            {
                return this.szornyMagassag;
            }

            set
            {
                this.szornyMagassag = value;
            }
        }

        /// <summary>
        /// Gets or sets : megadja, hogy meddig kell elszámolni, miután létrehozhatunk egy új random PalyaTartalom elemet a pályára
        /// </summary>
        public int JatekHaladasiSzam
        {
            get
            {
                return this.jatekHaladasiSzam;
            }

            set
            {
                this.jatekHaladasiSzam = value;
            }
        }

        /// <summary>
        /// Gets or sets : megadja, hogy meliyk indexű menüelem van kiválasztva
        /// </summary>
        public int KijeloltMenuElemSzama
        {
            get
            {
                return this.kijeloltMenuElemSzama;
            }

            set
            {
                this.kijeloltMenuElemSzama = value;
            }
        }

        /// <summary>
        /// Gets or sets : megadja,hogy mennyi a játék eredeti haladási száma. beállítás segítségével ezt kell módosítani, ha nehézségi szintet akarunk beállítani
        /// </summary>
        public int JatekEredetiHaladasiSzam
        {
            get
            {
                return this.jatekEredetiHaladasiSzam;
            }

            set
            {
                this.jatekEredetiHaladasiSzam = value;
            }
        }
    }
}
