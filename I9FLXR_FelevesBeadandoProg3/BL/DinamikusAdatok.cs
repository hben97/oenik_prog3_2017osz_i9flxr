﻿// <copyright file="DinamikusAdatok.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace I9Flxr_FelevesBeadandoProg3.BL
{
    /// <summary>
    /// A ViewModell hozzáférhetősége miatt létezik csak, hogy globálisan hozzáférjünk
    /// </summary>
    public static class DinamikusAdatok
    {
        private static ViewModel viewModel = new ViewModel();

        /// <summary>
        /// Gets or sets : viewmodell maga, ami a változó adatokat tárolj a a játékban
        /// </summary>
        public static ViewModel ViewModel
        {
            get { return viewModel; } set { viewModel = value; }
        }
    }
}
